package com.ruoyi.web.controller.system;

import java.io.IOException;

import com.ruoyi.common.core.domain.entity.*;
import com.ruoyi.common.enums.UserTypeEnum;
import com.ruoyi.system.mapper.AdminMapper;
import com.ruoyi.system.mapper.StudentMapper;
import com.ruoyi.system.mapper.TeacherMapper;
import com.ruoyi.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.service.ISysUserService;

import javax.annotation.Resource;

/**
 * 个人信息 业务处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/user/profile")
public class SysProfileController extends BaseController {

    @Autowired
    private IUserService userService;

    @Resource
    private AdminMapper adminMapper;

    @Resource
    private TeacherMapper teacherMapper;

    @Resource
    private StudentMapper studentMapper;

    @Autowired
    private TokenService tokenService;

    /**
     * 个人信息
     */
    @GetMapping
    public AjaxResult profile() {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        User user = loginUser.getUser();
        //根据用户类型查询不同的用户表 0-管理员 1-导师 2-学生
        if (user.getUserType().intValue() == UserTypeEnum.ADMIN.getValue()) {
            Admin admin = adminMapper.selectAdminByUserId(user.getUserId());
            user.setEmail(admin.getEmail());
            user.setPhone(admin.getTel());
            user.setRealName(admin.getName());
            user.setNo(admin.getNo());
            user.setSex(admin.getSex());
        } else if (user.getUserType().intValue() == UserTypeEnum.TEACHER.getValue()) {
            Teacher teacher = teacherMapper.selectTeacherByUserId(user.getUserId());
            user.setEmail(teacher.getEmail());
            user.setRealName(teacher.getName());
            user.setPhone(teacher.getTel());
            user.setCollegeId(teacher.getCollegeId());
            user.setNo(teacher.getNo());
            user.setProfessionalTitle(teacher.getProfessionalTitle());
            user.setPreQualification(teacher.getPreQualification());
            user.setSex(teacher.getSex());
        } else if (user.getUserType().intValue() == UserTypeEnum.STUDENT.getValue()) {
            Student student = studentMapper.selectStudentByUserId(user.getUserId());
            user.setUserId(student.getUserId());
            user.setEmail(student.getEmail());
            user.setRealName(student.getName());
            user.setPhone(student.getTel());
            user.setCollegeId(student.getCollegeId());
            user.setMajorId(student.getMajorId());
            user.setNo(student.getNo());
            user.setSex(student.getSex());
        }
        AjaxResult ajax = AjaxResult.success(user);
        ajax.put("roleGroup", userService.selectUserRoleGroup(loginUser.getUsername()));
        return ajax;
    }

    /**
     * 修改用户
     */
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult updateProfile(@RequestBody User user) {
        if (userService.updateUserProfile(user) > 0) {
            LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
            // todo 更新缓存用户信息
//            loginUser.getUser().setNickName(user.getNickName());
//            loginUser.getUser().setPhonenumber(user.getPhonenumber());
//            loginUser.getUser().setEmail(user.getEmail());
//            loginUser.getUser().setSex(user.getSex());
            tokenService.setLoginUser(loginUser);
            return AjaxResult.success();
        }
        return AjaxResult.error("修改个人信息异常，请联系管理员");
    }

    /**
     * 重置密码
     */
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PutMapping("/updatePwd")
    public AjaxResult updatePwd(String oldPassword, String newPassword) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userName = loginUser.getUsername();
        String password = loginUser.getPassword();
        if (!SecurityUtils.matchesPassword(oldPassword, password)) {
            return AjaxResult.error("修改密码失败，旧密码错误");
        }
        if (SecurityUtils.matchesPassword(newPassword, password)) {
            return AjaxResult.error("新密码不能与旧密码相同");
        }
        if (userService.resetUserPwd(userName, SecurityUtils.encryptPassword(newPassword)) > 0) {
            // 更新缓存用户密码
            loginUser.getUser().setPassword(SecurityUtils.encryptPassword(newPassword));
            tokenService.setLoginUser(loginUser);
            return AjaxResult.success();
        }
        return AjaxResult.error("修改密码异常，请联系管理员");
    }

    /**
     * 头像上传
     */
    @Log(title = "用户头像", businessType = BusinessType.UPDATE)
    @PostMapping("/avatar")
    public AjaxResult avatar(@RequestParam("avatarfile") MultipartFile file) throws IOException {
        if (!file.isEmpty()) {
            LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
            String avatar = FileUploadUtils.upload(RuoYiConfig.getAvatarPath(), file);
            if (userService.updateUserAvatar(loginUser.getUsername(), avatar)) {
                AjaxResult ajax = AjaxResult.success();
                ajax.put("imgUrl", avatar);
                // 更新缓存用户头像
                loginUser.getUser().setAvatar(avatar);
                tokenService.setLoginUser(loginUser);
                return ajax;
            }
        }
        return AjaxResult.error("上传图片异常，请联系管理员");
    }
}
