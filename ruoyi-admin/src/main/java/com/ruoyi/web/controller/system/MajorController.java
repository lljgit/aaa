package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.Major;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.service.IMajorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 专业信息Controller
 *
 * @author ruoyi
 * @date 2021-04-16
 */
@RestController
@RequestMapping("/system/major")
public class MajorController extends BaseController {
    @Autowired
    private IMajorService majorService;

    /**
     * 获取某学院下的所有专业
     */
    @GetMapping(value = "/byCollegeId/{collegeId}")
    public AjaxResult getMajorByCollegeId(@PathVariable("collegeId") Integer collegeId) {
        return AjaxResult.success(majorService.getMajorByCollegeId(collegeId));
    }

    /**
     * 查询专业信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:major:list')")
    @GetMapping("/list")
    public TableDataInfo list(Major major) {
        startPage();
        List<Major> list = majorService.selectMajorList(major);
        return getDataTable(list);
    }

    /**
     * 导出专业信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:major:export')")
    @Log(title = "专业信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Major major) {
        List<Major> list = majorService.selectMajorList(major);
        ExcelUtil<Major> util = new ExcelUtil<Major>(Major.class);
        return util.exportExcel(list, "专业信息数据");
    }

    /**
     * 获取专业信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:major:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(majorService.selectMajorById(id));
    }

    /**
     * 新增专业信息
     */
    @PreAuthorize("@ss.hasPermi('system:major:add')")
    @Log(title = "专业信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Major major) {
        //校验学院名称是否重复
        if (UserConstants.NOT_UNIQUE.equals(majorService.checkMajorNameUnique(major)))
        {
            return AjaxResult.error("新增专业'" + major.getMajorName() + "'失败，学院名称已存在");
        }
        //校验学院编号是否重复
        if (UserConstants.NOT_UNIQUE.equals(majorService.checkMajorNoUnique(major)))
        {
            return AjaxResult.error("新增专业'" + major.getMajorName() + "'失败，学院编号已存在");
        }
        return toAjax(majorService.insertMajor(major));
    }

    /**
     * 修改专业信息
     */
    @PreAuthorize("@ss.hasPermi('system:major:edit')")
    @Log(title = "专业信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Major major) {
        return toAjax(majorService.updateMajor(major));
    }

    /**
     * 删除专业信息
     */
    @PreAuthorize("@ss.hasPermi('system:major:remove')")
    @Log(title = "专业信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(majorService.deleteMajorByIds(ids));
    }
}