package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.College;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.service.ICollegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 学院信息Controller
 *
 * @author ruoyi
 * @date 2021-04-16
 */
@RestController
@RequestMapping("/system/college")
public class CollegeController extends BaseController {
    @Autowired
    private ICollegeService collegeService;

    /**
     * 查询学院信息列表
     */
    @GetMapping("/getList")
    public AjaxResult getList() {
        List<College> list = collegeService.getCollegeList();
        return AjaxResult.success(list);
    }

    /**
     * 查询学院信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:college:list')")
    @GetMapping("/list")
    public TableDataInfo list(College college) {
        startPage();
        List<College> list = collegeService.selectCollegeList(college);
        return getDataTable(list);
    }

    /**
     * 导出学院信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:college:export')")
    @Log(title = "学院信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(College college) {
        List<College> list = collegeService.selectCollegeList(college);
        ExcelUtil<College> util = new ExcelUtil<College>(College.class);
        return util.exportExcel(list, "学院信息数据");
    }

    /**
     * 获取学院信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:college:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(collegeService.selectCollegeById(id));
    }

    /**
     * 新增学院信息
     */
    @PreAuthorize("@ss.hasPermi('system:college:add')")
    @Log(title = "学院信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody College college) {
        //校验学院名称是否重复
        if (UserConstants.NOT_UNIQUE.equals(collegeService.checkCollegeNameUnique(college)))
        {
            return AjaxResult.error("新增学院'" + college.getCollegeName() + "'失败，学院名称已存在");
        }
        //校验学院编号是否重复
        if (UserConstants.NOT_UNIQUE.equals(collegeService.checkCollegeNoUnique(college)))
        {
            return AjaxResult.error("新增学院'" + college.getCollegeName() + "'失败，学院编号已存在");
        }
        college.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(collegeService.insertCollege(college));
    }

    /**
     * 修改学院信息
     */
    @PreAuthorize("@ss.hasPermi('system:college:edit')")
    @Log(title = "学院信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody College college) {
        //校验学院名称是否重复
        if (UserConstants.NOT_UNIQUE.equals(collegeService.checkCollegeNameUnique(college)))
        {
            return AjaxResult.error("修改学院'" + college.getCollegeName() + "'失败，学院名称已存在");
        }
        if (UserConstants.NOT_UNIQUE.equals(collegeService.checkCollegeNoUnique(college)))
        {
            return AjaxResult.error("修改学院'" + college.getCollegeName() + "'失败，学院编号已存在");
        }
        college.setCreateBy(SecurityUtils.getUsername());
        return toAjax(collegeService.updateCollege(college));
    }

    /**
     * 删除学院信息
     */
    @PreAuthorize("@ss.hasPermi('system:college:remove')")
    @Log(title = "学院信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(collegeService.deleteCollegeByIds(ids));
    }
}