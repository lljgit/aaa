package com.ruoyi.common.enums;

/**
 * 用户身份类型美剧
 */
public enum UserTypeEnum {
    ADMIN("管理员", 0), TEACHER("导师", 1), STUDENT("学生", 2);

    private final String name;
    private final Integer value;

    UserTypeEnum(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }
}
