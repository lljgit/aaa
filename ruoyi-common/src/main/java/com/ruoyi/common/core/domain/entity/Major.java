package com.ruoyi.common.core.domain.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 专业信息对象 major
 * 
 * @author ruoyi
 * @date 2021-04-16
 */
public class Major extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 专业ID */
    private Integer id;

    /** 专业编号 */
    @Excel(name = "专业编号")
    private String no;

    /** 专业名称 */
    @Excel(name = "专业名称")
    private String majorName;

    /** 学院id */
    @Excel(name = "学院id")
    private Integer collegeId;

    /** 学院名称 */
    @Excel(name = "学院名称")
    private String collegeName;

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }

    public void setNo(String no)
    {
        this.no = no;
    }

    public String getNo() 
    {
        return no;
    }
    public void setMajorName(String majorName) 
    {
        this.majorName = majorName;
    }

    public String getMajorName() 
    {
        return majorName;
    }
    public void setCollegeId(Integer collegeId) 
    {
        this.collegeId = collegeId;
    }

    public Integer getCollegeId() 
    {
        return collegeId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("no", getNo())
            .append("majorName", getMajorName())
            .append("collegeId", getCollegeId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}