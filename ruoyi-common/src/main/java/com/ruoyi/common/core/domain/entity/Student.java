package com.ruoyi.common.core.domain.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 学生对象 student
 * 
 * @author ruoyi
 * @date 2021-04-16
 */
public class Student extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 学生ID */
    private Integer studentId;

    /** 对应用户表id */
    @Excel(name = "对应用户表id")
    private Integer userId;

    /** 学号 */
    @Excel(name = "学号")
    private String no;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 性别 1-男 2-女 */
    @Excel(name = "性别 1-男 2-女")
    private Integer sex;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String tel;

    /** 学院id */
    @Excel(name = "学院id")
    private Integer collegeId;

    /** 专业id */
    @Excel(name = "专业id")
    private Integer majorId;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    public void setStudentId(Integer studentId) 
    {
        this.studentId = studentId;
    }

    public Integer getStudentId() 
    {
        return studentId;
    }
    public void setUserId(Integer userId) 
    {
        this.userId = userId;
    }

    public Integer getUserId() 
    {
        return userId;
    }
    public void setNo(String no) 
    {
        this.no = no;
    }

    public String getNo() 
    {
        return no;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setSex(Integer sex) 
    {
        this.sex = sex;
    }

    public Integer getSex() 
    {
        return sex;
    }
    public void setTel(String tel) 
    {
        this.tel = tel;
    }

    public String getTel() 
    {
        return tel;
    }
    public void setCollegeId(Integer collegeId) 
    {
        this.collegeId = collegeId;
    }

    public Integer getCollegeId() 
    {
        return collegeId;
    }
    public void setMajorId(Integer majorId) 
    {
        this.majorId = majorId;
    }

    public Integer getMajorId() 
    {
        return majorId;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("studentId", getStudentId())
            .append("userId", getUserId())
            .append("no", getNo())
            .append("name", getName())
            .append("sex", getSex())
            .append("tel", getTel())
            .append("collegeId", getCollegeId())
            .append("majorId", getMajorId())
            .append("email", getEmail())
            .toString();
    }
}