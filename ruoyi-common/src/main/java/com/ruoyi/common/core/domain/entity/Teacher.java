package com.ruoyi.common.core.domain.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 教师对象 teacher
 *
 * @author ruoyi
 * @date 2021-04-16
 */
public class Teacher extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 教师ID
     */
    private Integer teacherId;

    /**
     * 对应用户表id
     */
    @Excel(name = "对应用户表id")
    private Integer userId;

    /**
     * 工号
     */
    @Excel(name = "工号")
    private String no;

    /**
     * 姓名
     */
    @Excel(name = "姓名")
    private String name;

    /**
     * 性别 1-男 2-女
     */
    @Excel(name = "性别 1-男 2-女")
    private Integer sex;

    /**
     * 联系方式
     */
    @Excel(name = "联系方式")
    private String tel;

    /**
     * 学院id
     */
    @Excel(name = "学院id")
    private Integer collegeId;

    /**
     * 职称
     */
    @Excel(name = "职称")
    private String professionalTitle;

    /**
     * 邮箱
     */
    @Excel(name = "邮箱")
    private String email;

    /**
     * 答辩权限（1参与答辩 2不参与答辩）
     */
    @Excel(name = "答辩权限", readConverterExp = "1=参与答辩,2=不参与答辩")
    private String preQualification;


    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getNo() {
        return no;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getSex() {
        return sex;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTel() {
        return tel;
    }

    public void setCollegeId(Integer collegeId) {
        this.collegeId = collegeId;
    }

    public Integer getCollegeId() {
        return collegeId;
    }

    public void setProfessionalTitle(String professionalTitle) {
        this.professionalTitle = professionalTitle;
    }

    public String getProfessionalTitle() {
        return professionalTitle;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPreQualification(String preQualification) {
        this.preQualification = preQualification;
    }

    public String getPreQualification() {
        return preQualification;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("teacherId", getTeacherId())
                .append("userId", getUserId())
                .append("no", getNo())
                .append("name", getName())
                .append("sex", getSex())
                .append("tel", getTel())
                .append("collegeId", getCollegeId())
                .append("professionalTitle", getProfessionalTitle())
                .append("email", getEmail())
                .append("preQualification", getPreQualification())
                .toString();
    }
}