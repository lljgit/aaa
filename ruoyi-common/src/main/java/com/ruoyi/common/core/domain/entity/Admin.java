package com.ruoyi.common.core.domain.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 管理员对象 admin
 *
 * @author ruoyi
 * @date 2021-04-16
 */
public class Admin extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 管理员ID
     */
    private Integer adminId;

    /**
     * 对应用户表id
     */
    @Excel(name = "对应用户表id")
    private Integer userId;

    /**
     * 编号
     */
    @Excel(name = "编号")
    private String no;

    /**
     * 姓名
     */
    @Excel(name = "姓名")
    private String name;

    /**
     * 性别 1-男 2-女
     */
    private Integer sex;

    /**
     * 电话
     */
    @Excel(name = "电话")
    private String tel;

    /**
     * 邮箱
     */
    @Excel(name = "邮箱")
    private String email;

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getNo() {
        return no;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTel() {
        return tel;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("adminId", getAdminId())
                .append("userId", getUserId())
                .append("no", getNo())
                .append("name", getName())
                .append("tel", getTel())
                .append("email", getEmail())
                .toString();
    }
}