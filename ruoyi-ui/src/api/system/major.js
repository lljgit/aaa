import request from '@/utils/request'

// 查询所有学院
export function getCollegeList() {
  return request({
    url: '/system/college/getList' ,
    method: 'get'
  })
}

// 查询专业信息详细
export function getMajorByCollegeId(collegeId) {
  return request({
    url: '/system/major/byCollegeId/' + collegeId,
    method: 'get'
  })
}

// 查询专业信息列表
export function listMajor(query) {
  return request({
    url: '/system/major/list',
    method: 'get',
    params: query
  })
}

// 查询专业信息详细
export function getMajor(id) {
  return request({
    url: '/system/major/' + id,
    method: 'get'
  })
}

// 新增专业信息
export function addMajor(data) {
  return request({
    url: '/system/major',
    method: 'post',
    data: data
  })
}

// 修改专业信息
export function updateMajor(data) {
  return request({
    url: '/system/major',
    method: 'put',
    data: data
  })
}

// 删除专业信息
export function delMajor(id) {
  return request({
    url: '/system/major/' + id,
    method: 'delete'
  })
}

// 导出专业信息
export function exportMajor(query) {
  return request({
    url: '/system/major/export',
    method: 'get',
    params: query
  })
}
