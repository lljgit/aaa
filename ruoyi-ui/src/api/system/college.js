import request from '@/utils/request'

// 查询学院信息列表
export function listCollege(query) {
  return request({
    url: '/system/college/list',
    method: 'get',
    params: query
  })
}

// 查询学院信息详细
export function getCollege(id) {
  return request({
    url: '/system/college/' + id,
    method: 'get'
  })
}

// 新增学院信息
export function addCollege(data) {
  return request({
    url: '/system/college',
    method: 'post',
    data: data
  })
}

// 修改学院信息
export function updateCollege(data) {
  return request({
    url: '/system/college',
    method: 'put',
    data: data
  })
}

// 删除学院信息
export function delCollege(id) {
  return request({
    url: '/system/college/' + id,
    method: 'delete'
  })
}

// 导出学院信息
export function exportCollege(query) {
  return request({
    url: '/system/college/export',
    method: 'get',
    params: query
  })
}
