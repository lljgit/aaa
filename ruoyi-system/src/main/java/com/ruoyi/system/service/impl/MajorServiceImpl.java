package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.College;
import com.ruoyi.common.core.domain.entity.Major;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.mapper.MajorMapper;
import com.ruoyi.system.service.IMajorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 专业信息Service业务层处理
 *
 * @author ruoyi
 * @date 2021-04-16
 */
@Service
public class MajorServiceImpl implements IMajorService {
    @Autowired
    private MajorMapper majorMapper;

    /**
     * 查询专业信息
     *
     * @param id 专业信息ID
     * @return 专业信息
     */
    @Override
    public Major selectMajorById(Integer id) {
        return majorMapper.selectMajorById(id);
    }

    /**
     * 查询专业信息列表
     *
     * @param major 专业信息
     * @return 专业信息
     */
    @Override
    public List<Major> selectMajorList(Major major) {
        return majorMapper.selectMajorList(major);
    }

    /**
     * 新增专业信息
     *
     * @param major 专业信息
     * @return 结果
     */
    @Override
    public int insertMajor(Major major) {
        major.setCreateTime(DateUtils.getNowDate());
        return majorMapper.insertMajor(major);
    }

    /**
     * 修改专业信息
     *
     * @param major 专业信息
     * @return 结果
     */
    @Override
    public int updateMajor(Major major) {
        major.setUpdateTime(DateUtils.getNowDate());
        return majorMapper.updateMajor(major);
    }

    /**
     * 批量删除专业信息
     *
     * @param ids 需要删除的专业信息ID
     * @return 结果
     */
    @Override
    public int deleteMajorByIds(Integer[] ids) {
        return majorMapper.deleteMajorByIds(ids);
    }

    /**
     * 删除专业信息信息
     *
     * @param id 专业信息ID
     * @return 结果
     */
    @Override
    public int deleteMajorById(Integer id) {
        return majorMapper.deleteMajorById(id);
    }

    @Override
    public String checkMajorNameUnique(Major major) {
        Major info = majorMapper.checkMajorNameUnique(major.getMajorName());
        if (StringUtils.isNotNull(info) && (major.getId() == null || info.getId().intValue() != major.getId())) {
            return UserConstants.NOT_UNIQUE;
        }

        return UserConstants.UNIQUE;
    }

    @Override
    public String checkMajorNoUnique(Major major) {
        Major info = majorMapper.checkMajorNoUnique(major.getNo());
        if (StringUtils.isNotNull(info) && (major.getId() == null || info.getId().intValue() != major.getId())) {
            return UserConstants.NOT_UNIQUE;
        }

        return UserConstants.UNIQUE;
    }

    @Override
    public List<Major> getMajorByCollegeId(Integer collegeId) {
        return majorMapper.getMajorByCollegeId(collegeId);
    }
}