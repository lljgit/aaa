package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.entity.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AdminMapper;
import com.ruoyi.system.service.IAdminService;

/**
 * 管理员Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-04-16
 */
@Service
public class AdminServiceImpl implements IAdminService 
{
    @Autowired
    private AdminMapper adminMapper;

    /**
     * 查询管理员
     * 
     * @param adminId 管理员ID
     * @return 管理员
     */
    @Override
    public Admin selectAdminById(Integer adminId)
    {
        return adminMapper.selectAdminById(adminId);
    }

    /**
     * 查询管理员列表
     * 
     * @param admin 管理员
     * @return 管理员
     */
    @Override
    public List<Admin> selectAdminList(Admin admin)
    {
        return adminMapper.selectAdminList(admin);
    }

    /**
     * 新增管理员
     * 
     * @param admin 管理员
     * @return 结果
     */
    @Override
    public int insertAdmin(Admin admin)
    {
        return adminMapper.insertAdmin(admin);
    }

    /**
     * 修改管理员
     * 
     * @param admin 管理员
     * @return 结果
     */
    @Override
    public int updateAdmin(Admin admin)
    {
        return adminMapper.updateAdmin(admin);
    }

    /**
     * 批量删除管理员
     * 
     * @param adminIds 需要删除的管理员ID
     * @return 结果
     */
    @Override
    public int deleteAdminByIds(Integer[] adminIds)
    {
        return adminMapper.deleteAdminByIds(adminIds);
    }

    /**
     * 删除管理员信息
     * 
     * @param adminId 管理员ID
     * @return 结果
     */
    @Override
    public int deleteAdminById(Integer adminId)
    {
        return adminMapper.deleteAdminById(adminId);
    }
}