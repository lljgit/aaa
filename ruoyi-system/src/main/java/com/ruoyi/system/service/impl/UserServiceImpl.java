package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.*;
import com.ruoyi.common.enums.UserTypeEnum;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.UserRole;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户信息Service业务层处理
 *
 * @author ruoyi
 * @date 2021-04-16
 */
@Service
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Resource
    private UserRoleMapper userRoleMapper;

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private AdminMapper adminMapper;
    @Resource
    private TeacherMapper teacherMapper;
    @Resource
    private StudentMapper studentMapper;

    /**
     * 查询用户信息
     *
     * @param userId 用户信息ID
     * @return 用户信息
     */
    @Override
    public User selectUserById(Integer userId) {
        User user = userMapper.selectUserById(userId);
        if (user.getUserType().intValue() == UserTypeEnum.ADMIN.getValue()) {
            Admin admin = adminMapper.selectAdminByUserId(userId);
            user.setEmail(admin.getEmail());
            user.setPhone(admin.getTel());
            user.setRealName(admin.getName());
            user.setNo(admin.getNo());
            user.setSex(admin.getSex());
        } else if (user.getUserType().intValue() == UserTypeEnum.TEACHER.getValue()) {
            Teacher teacher = teacherMapper.selectTeacherByUserId(userId);
            user.setEmail(teacher.getEmail());
            user.setRealName(teacher.getName());
            user.setPhone(teacher.getTel());
            user.setCollegeId(teacher.getCollegeId());
            user.setNo(teacher.getNo());
            user.setProfessionalTitle(teacher.getProfessionalTitle());
            user.setPreQualification(teacher.getPreQualification());
            user.setSex(teacher.getSex());
        } else if (user.getUserType().intValue() == UserTypeEnum.STUDENT.getValue()) {
            Student student = studentMapper.selectStudentByUserId(userId);
            user.setUserId(student.getUserId());
            user.setEmail(student.getEmail());
            user.setRealName(student.getName());
            user.setPhone(student.getTel());
            user.setCollegeId(student.getCollegeId());
            user.setMajorId(student.getMajorId());
            user.setNo(student.getNo());
            user.setSex(student.getSex());
        }
        return user;
    }

    /**
     * 查询用户信息列表
     *
     * @param user 用户信息
     * @return 用户信息
     */
    @Override
    public List<User> selectUserList(User user) {
        if (user.getUserType() == null) {
            throw new CustomException(String.format("userType不能为空"));
        }
        List<User> userList = null;
        if (user.getUserType().intValue() == UserTypeEnum.ADMIN.getValue()) {
            userList = userMapper.selectAdminList(user);
        } else if (user.getUserType().intValue() == UserTypeEnum.TEACHER.getValue()) {
            userList = userMapper.selectTeacherList(user);
        } else if (user.getUserType().intValue() == UserTypeEnum.STUDENT.getValue()) {
            userList = userMapper.selectStudentList(user);
        } else {
            throw new CustomException(String.format("userType传参有误"));
        }
        return userList;
    }

    /**
     * 新增保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertUser(User user) {
        // 新增用户信息
        int rows = userMapper.insertUser(user);
        if (user.getUserType().intValue() == UserTypeEnum.ADMIN.getValue()) {
            Admin admin = new Admin();
            admin.setUserId(user.getUserId());
            admin.setEmail(user.getEmail());
            admin.setTel(user.getPhone());
            admin.setName(user.getRealName());
            admin.setNo(user.getNo());
            admin.setSex(user.getSex());
            adminMapper.insertAdmin(admin);
        } else if (user.getUserType().intValue() == UserTypeEnum.TEACHER.getValue()) {
            Teacher teacher = new Teacher();
            teacher.setUserId(user.getUserId());
            teacher.setEmail(user.getEmail());
            teacher.setName(user.getRealName());
            teacher.setTel(user.getPhone());
            teacher.setCollegeId(user.getCollegeId());
            teacher.setNo(user.getNo());
            teacher.setProfessionalTitle(user.getProfessionalTitle());
            teacher.setPreQualification(user.getPreQualification());
            teacher.setSex(user.getSex());
            teacherMapper.insertTeacher(teacher);
        } else if (user.getUserType().intValue() == UserTypeEnum.STUDENT.getValue()) {
            Student student = new Student();
            student.setUserId(user.getUserId());
            student.setEmail(user.getEmail());
            student.setName(user.getRealName());
            student.setTel(user.getPhone());
            student.setCollegeId(user.getCollegeId());
            student.setMajorId(user.getMajorId());
            student.setNo(user.getNo());
            student.setSex(user.getSex());
            studentMapper.insertStudent(student);
        }
        // 新增用户与角色管理
        insertUserRole(user);
        return rows;
    }

    /**
     * 新增用户角色信息
     *
     * @param user 用户对象
     */
    public void insertUserRole(User user) {
        Long[] roles = user.getRoleIds();
        if (StringUtils.isNotNull(roles)) {
            // 新增用户与角色管理
            List<UserRole> list = new ArrayList<>();
            for (Long roleId : roles) {
                UserRole ur = new UserRole();
                ur.setUserId(user.getUserId());
                ur.setRoleId(roleId);
                list.add(ur);
            }
            if (list.size() > 0) {
                userRoleMapper.batchUserRole(list);
            }
        }
    }

    /**
     * 修改用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int updateUser(User user) {
        Integer userId = user.getUserId();
        // 删除用户与角色关联
        userRoleMapper.deleteUserRoleByUserId(userId);
        // 新增用户与角色管理
        insertUserRole(user);
        //修改用户基础信息
        int rows = userMapper.updateUser(user);
        if (user.getUserType().intValue() == UserTypeEnum.ADMIN.getValue()) {
            Admin admin = new Admin();
            admin.setUserId(user.getUserId());
            admin.setEmail(user.getEmail());
            admin.setTel(user.getPhone());
            admin.setName(user.getRealName());
            admin.setNo(user.getNo());
            admin.setSex(user.getSex());
            adminMapper.updateAdminByUserId(admin);
        } else if (user.getUserType().intValue() == UserTypeEnum.TEACHER.getValue()) {
            Teacher teacher = new Teacher();
            teacher.setUserId(user.getUserId());
            teacher.setEmail(user.getEmail());
            teacher.setName(user.getRealName());
            teacher.setTel(user.getPhone());
            teacher.setCollegeId(user.getCollegeId());
            teacher.setNo(user.getNo());
            teacher.setProfessionalTitle(user.getProfessionalTitle());
            teacher.setPreQualification(user.getPreQualification());
            teacher.setSex(user.getSex());
            teacherMapper.updateTeacherByUserId(teacher);
        } else if (user.getUserType().intValue() == UserTypeEnum.STUDENT.getValue()) {
            Student student = new Student();
            student.setUserId(user.getUserId());
            student.setEmail(user.getEmail());
            student.setName(user.getRealName());
            student.setTel(user.getPhone());
            student.setCollegeId(user.getCollegeId());
            student.setMajorId(user.getMajorId());
            student.setNo(user.getNo());
            student.setSex(user.getSex());
            studentMapper.updateStudentByUserId(student);
        }
        return rows;
    }

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserByIds(Integer[] userIds) {
        // 删除用户与角色关联
        userRoleMapper.deleteUserRole(userIds);
        int rows = userMapper.deleteUserByIds(userIds);
        adminMapper.deleteAdminByUserIds(userIds);
        teacherMapper.deleteTeacherByUserIds(userIds);
        studentMapper.deleteStudentByUserIds(userIds);
        return rows;
    }

    /**
     * 删除用户信息信息
     *
     * @param userId 用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserById(Integer userId) {
        int rows = userMapper.deleteUserById(userId);
        adminMapper.deleteAdminByUserId(userId);
        teacherMapper.deleteTeacherByUserId(userId);
        studentMapper.deleteStudentByUserId(userId);
        return rows;
    }

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    @Override
    public User selectUserByUserName(String userName) {
        return userMapper.selectUserByUserName(userName);
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param userName 用户名称
     * @return 结果
     */
    @Override
    public String checkUserNameUnique(String userName) {
        int count = userMapper.checkUserNameUnique(userName);
        if (count > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int resetPwd(User user) {
        return userMapper.updateUser(user);
    }

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int updateUserStatus(User user) {
        return userMapper.updateUser(user);
    }

    /**
     * 查询用户所属角色组
     *
     * @param userName 用户名
     * @return 结果
     */
    @Override
    public String selectUserRoleGroup(String userName) {
        List<Role> list = roleMapper.selectRolesByUserName(userName);
        StringBuffer idsStr = new StringBuffer();
        for (Role role : list) {
            idsStr.append(role.getRoleName()).append(",");
        }
        if (StringUtils.isNotEmpty(idsStr.toString())) {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int updateUserProfile(User user) {
        userMapper.updateUser(user);
        int count = 0;
        //根据不同的用户身份修改不同的用户表信息
        if (user.getUserType().intValue() == UserTypeEnum.ADMIN.getValue()) {
            //管理员
            Admin admin = new Admin();
            admin.setUserId(user.getUserId());
            admin.setName(user.getRealName());
            admin.setTel(user.getPhone());
            admin.setEmail(user.getEmail());
            count = adminMapper.updateAdminByUserId(admin);
        } else if (user.getUserType().intValue() == UserTypeEnum.TEACHER.getValue()) {
            //导师
            Teacher teacher = new Teacher();
            teacher.setUserId(user.getUserId());
            teacher.setName(user.getRealName());
            teacher.setTel(user.getPhone());
            teacher.setEmail(user.getEmail());
            count = teacherMapper.updateTeacherByUserId(teacher);
        } else if (user.getUserType().intValue() == UserTypeEnum.STUDENT.getValue()) {
            //学生
            Student student = new Student();
            student.setUserId(user.getUserId());
            student.setName(user.getRealName());
            student.setTel(user.getPhone());
            student.setEmail(user.getEmail());
            count = studentMapper.updateStudentByUserId(student);
        }
        return count;
    }

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    @Override
    public int resetUserPwd(String userName, String password) {
        return userMapper.resetUserPwd(userName, password);
    }

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar   头像地址
     * @return 结果
     */
    @Override
    public boolean updateUserAvatar(String userName, String avatar) {
        return userMapper.updateUserAvatar(userName, avatar) > 0;
    }

}