package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.entity.Major;

import java.util.List;

/**
 * 专业信息Service接口
 *
 * @author ruoyi
 * @date 2021-04-16
 */
public interface IMajorService {
    /**
     * 查询专业信息
     *
     * @param id 专业信息ID
     * @return 专业信息
     */
    public Major selectMajorById(Integer id);

    /**
     * 查询专业信息列表
     *
     * @param major 专业信息
     * @return 专业信息集合
     */
    public List<Major> selectMajorList(Major major);

    /**
     * 新增专业信息
     *
     * @param major 专业信息
     * @return 结果
     */
    public int insertMajor(Major major);

    /**
     * 修改专业信息
     *
     * @param major 专业信息
     * @return 结果
     */
    public int updateMajor(Major major);

    /**
     * 批量删除专业信息
     *
     * @param ids 需要删除的专业信息ID
     * @return 结果
     */
    public int deleteMajorByIds(Integer[] ids);

    /**
     * 删除专业信息信息
     *
     * @param id 专业信息ID
     * @return 结果
     */
    public int deleteMajorById(Integer id);

    String checkMajorNameUnique(Major major);

    String checkMajorNoUnique(Major major);

    List<Major> getMajorByCollegeId(Integer collegeId);
}