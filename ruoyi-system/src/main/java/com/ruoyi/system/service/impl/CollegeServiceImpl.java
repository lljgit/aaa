package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.College;
import com.ruoyi.common.core.domain.entity.Menu;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.mapper.CollegeMapper;
import com.ruoyi.system.service.ICollegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 学院信息Service业务层处理
 *
 * @author ruoyi
 * @date 2021-04-16
 */
@Service
public class CollegeServiceImpl implements ICollegeService {
    @Autowired
    private CollegeMapper collegeMapper;

    /**
     * 查询学院信息
     *
     * @param id 学院信息ID
     * @return 学院信息
     */
    @Override
    public College selectCollegeById(Integer id) {
        return collegeMapper.selectCollegeById(id);
    }

    /**
     * 查询学院信息列表
     *
     * @param college 学院信息
     * @return 学院信息
     */
    @Override
    public List<College> selectCollegeList(College college) {
        return collegeMapper.selectCollegeList(college);
    }

    /**
     * 新增学院信息
     *
     * @param college 学院信息
     * @return 结果
     */
    @Override
    public int insertCollege(College college) {
        return collegeMapper.insertCollege(college);
    }

    /**
     * 修改学院信息
     *
     * @param college 学院信息
     * @return 结果
     */
    @Override
    public int updateCollege(College college) {
        college.setUpdateTime(DateUtils.getNowDate());
        return collegeMapper.updateCollege(college);
    }

    /**
     * 批量删除学院信息
     *
     * @param ids 需要删除的学院信息ID
     * @return 结果
     */
    @Override
    public int deleteCollegeByIds(Integer[] ids) {
        return collegeMapper.deleteCollegeByIds(ids);
    }

    /**
     * 删除学院信息信息
     *
     * @param id 学院信息ID
     * @return 结果
     */
    @Override
    public int deleteCollegeById(Integer id) {
        return collegeMapper.deleteCollegeById(id);
    }

    /**
     * 校验学院名称是否唯一
     *
     * @param college
     * @return
     */
    @Override
    public String checkCollegeNameUnique(College college) {
        College info = collegeMapper.checkCollegeNameUnique(college.getCollegeName());
        if (StringUtils.isNotNull(info) && (college.getId() == null || info.getId().intValue() != college.getId())) {
            return UserConstants.NOT_UNIQUE;
        }

        return UserConstants.UNIQUE;
    }

    @Override
    public String checkCollegeNoUnique(College college) {
        College info = collegeMapper.checkCollegeNoUnique(college.getNo());
        if (StringUtils.isNotNull(info) && (college.getId() == null || info.getId().intValue() != college.getId())) {
            return UserConstants.NOT_UNIQUE;
        }

        return UserConstants.UNIQUE;
    }

    @Override
    public List<College> getCollegeList() {
        return collegeMapper.getCollegeList();
    }
}