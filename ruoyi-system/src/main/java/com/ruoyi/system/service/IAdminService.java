package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.entity.Admin;

import java.util.List;

/**
 * 管理员Service接口
 * 
 * @author ruoyi
 * @date 2021-04-16
 */
public interface IAdminService 
{
    /**
     * 查询管理员
     * 
     * @param adminId 管理员ID
     * @return 管理员
     */
    public Admin selectAdminById(Integer adminId);

    /**
     * 查询管理员列表
     * 
     * @param admin 管理员
     * @return 管理员集合
     */
    public List<Admin> selectAdminList(Admin admin);

    /**
     * 新增管理员
     * 
     * @param admin 管理员
     * @return 结果
     */
    public int insertAdmin(Admin admin);

    /**
     * 修改管理员
     * 
     * @param admin 管理员
     * @return 结果
     */
    public int updateAdmin(Admin admin);

    /**
     * 批量删除管理员
     * 
     * @param adminIds 需要删除的管理员ID
     * @return 结果
     */
    public int deleteAdminByIds(Integer[] adminIds);

    /**
     * 删除管理员信息
     * 
     * @param adminId 管理员ID
     * @return 结果
     */
    public int deleteAdminById(Integer adminId);
}