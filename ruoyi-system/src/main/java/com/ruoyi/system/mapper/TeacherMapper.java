package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.Teacher;

import java.util.List;

/**
 * 教师Mapper接口
 *
 * @author ruoyi
 * @date 2021-04-16
 */
public interface TeacherMapper {
    /**
     * 查询教师
     *
     * @param teacherId 教师ID
     * @return 教师
     */
    public Teacher selectTeacherById(Integer teacherId);

    /**
     * 查询教师列表
     *
     * @param teacher 教师
     * @return 教师集合
     */
    public List<Teacher> selectTeacherList(Teacher teacher);

    /**
     * 新增教师
     *
     * @param teacher 教师
     * @return 结果
     */
    public int insertTeacher(Teacher teacher);

    /**
     * 修改教师
     *
     * @param teacher 教师
     * @return 结果
     */
    public int updateTeacher(Teacher teacher);

    /**
     * 删除教师
     *
     * @param teacherId 教师ID
     * @return 结果
     */
    public int deleteTeacherById(Integer teacherId);

    /**
     * 批量删除教师
     *
     * @param teacherIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteTeacherByIds(Integer[] teacherIds);

    Teacher getByUserId(Integer userId);

    int updateTeacherByUserId(Teacher teacher);

    void deleteTeacherByUserIds(Integer[] userIds);

    void deleteTeacherByUserId(Integer userId);

    Teacher selectTeacherByUserId(Integer userId);
}