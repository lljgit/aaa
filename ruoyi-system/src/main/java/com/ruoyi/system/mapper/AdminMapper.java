package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.Admin;

import java.util.List;

/**
 * 管理员Mapper接口
 * 
 * @author ruoyi
 * @date 2021-04-16
 */
public interface AdminMapper 
{
    /**
     * 查询管理员
     * 
     * @param adminId 管理员ID
     * @return 管理员
     */
    public Admin selectAdminById(Integer adminId);

    /**
     * 查询管理员列表
     * 
     * @param admin 管理员
     * @return 管理员集合
     */
    public List<Admin> selectAdminList(Admin admin);

    /**
     * 新增管理员
     * 
     * @param admin 管理员
     * @return 结果
     */
    public int insertAdmin(Admin admin);

    /**
     * 修改管理员
     * 
     * @param admin 管理员
     * @return 结果
     */
    public int updateAdmin(Admin admin);

    /**
     * 删除管理员
     * 
     * @param adminId 管理员ID
     * @return 结果
     */
    public int deleteAdminById(Integer adminId);

    /**
     * 批量删除管理员
     * 
     * @param adminIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteAdminByIds(Integer[] adminIds);

    Admin getByUserId(Integer userId);

    int updateAdminByUserId(Admin admin);

    void deleteAdminByUserIds(Integer[] userIds);

    void deleteAdminByUserId(Integer userId);

    Admin selectAdminByUserId(Integer userId);
}