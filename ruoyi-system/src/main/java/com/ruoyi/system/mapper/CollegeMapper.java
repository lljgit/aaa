package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.common.core.domain.entity.College;

/**
 * 学院信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-04-16
 */
public interface CollegeMapper 
{
    /**
     * 查询学院信息
     * 
     * @param id 学院信息ID
     * @return 学院信息
     */
    public College selectCollegeById(Integer id);

    /**
     * 查询学院信息列表
     * 
     * @param college 学院信息
     * @return 学院信息集合
     */
    public List<College> selectCollegeList(College college);

    /**
     * 新增学院信息
     * 
     * @param college 学院信息
     * @return 结果
     */
    public int insertCollege(College college);

    /**
     * 修改学院信息
     * 
     * @param college 学院信息
     * @return 结果
     */
    public int updateCollege(College college);

    /**
     * 删除学院信息
     * 
     * @param id 学院信息ID
     * @return 结果
     */
    public int deleteCollegeById(Integer id);

    /**
     * 批量删除学院信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCollegeByIds(Integer[] ids);

    College checkCollegeNameUnique(String collegeName);

    College checkCollegeNoUnique(String no);

    List<College> getCollegeList();

}