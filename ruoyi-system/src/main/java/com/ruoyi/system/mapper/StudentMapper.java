package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.Student;

import java.util.List;

/**
 * 学生Mapper接口
 *
 * @author ruoyi
 * @date 2021-04-16
 */
public interface StudentMapper {
    /**
     * 查询学生
     *
     * @param studentId 学生ID
     * @return 学生
     */
    public Student selectStudentById(Integer studentId);

    /**
     * 查询学生列表
     *
     * @param student 学生
     * @return 学生集合
     */
    public List<Student> selectStudentList(Student student);

    /**
     * 新增学生
     *
     * @param student 学生
     * @return 结果
     */
    public int insertStudent(Student student);

    /**
     * 修改学生
     *
     * @param student 学生
     * @return 结果
     */
    public int updateStudent(Student student);

    /**
     * 删除学生
     *
     * @param studentId 学生ID
     * @return 结果
     */
    public int deleteStudentById(Integer studentId);

    /**
     * 批量删除学生
     *
     * @param studentIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteStudentByIds(Integer[] studentIds);

    Student getByUserId(Integer userId);

    int updateStudentByUserId(Student student);

    void deleteStudentByUserIds(Integer[] userIds);

    void deleteStudentByUserId(Integer userId);

    Student selectStudentByUserId(Integer userId);
}