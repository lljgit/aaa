package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.Role;

import java.util.List;

/**
 * 角色信息Mapper接口
 *
 * @author ruoyi
 * @date 2021-04-16
 */
public interface RoleMapper {
    /**
     * 查询角色信息
     *
     * @param roleId 角色信息ID
     * @return 角色信息
     */
    public Role selectRoleById(Long roleId);

    /**
     * 查询角色信息列表
     *
     * @param role 角色信息
     * @return 角色信息集合
     */
    public List<Role> selectRoleList(Role role);

    /**
     * 新增角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    public int insertRole(Role role);

    /**
     * 修改角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    public int updateRole(Role role);

    /**
     * 删除角色信息
     *
     * @param roleId 角色信息ID
     * @return 结果
     */
    public int deleteRoleById(Long roleId);

    /**
     * 批量删除角色信息
     *
     * @param roleIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteRoleByIds(Long[] roleIds);

    List<Role> selectRolePermissionByUserId(Integer userId);

    /**
     * 校验角色名称是否唯一
     *
     * @param roleName 角色名称
     * @return 角色信息
     */
    public Role checkRoleNameUnique(String roleName);

    /**
     * 根据用户ID获取角色选择框列表
     *
     * @param userId 用户ID
     * @return 选中角色ID列表
     */
    public List<Integer> selectRoleListByUserId(Integer userId);

    /**
     * 根据用户ID查询角色
     *
     * @param userName 用户名
     * @return 角色列表
     */
    public List<Role> selectRolesByUserName(String userName);

    /**
     * 校验角色权限是否唯一
     *
     * @param roleKey 角色权限
     * @return 角色信息
     */
    public Role checkRoleKeyUnique(String roleKey);
}