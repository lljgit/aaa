package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.Menu;
import com.ruoyi.common.core.domain.entity.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 菜单权限Mapper接口
 *
 * @author ruoyi
 * @date 2021-04-16
 */
public interface MenuMapper {
    /**
     * 查询菜单权限
     *
     * @param menuId 菜单权限ID
     * @return 菜单权限
     */
    public Menu selectMenuById(Integer menuId);

    /**
     * 查询菜单权限列表
     *
     * @param menu 菜单权限
     * @return 菜单权限集合
     */
    public List<Menu> selectMenuList(Menu menu);

    /**
     * 新增菜单权限
     *
     * @param menu 菜单权限
     * @return 结果
     */
    public int insertMenu(Menu menu);

    /**
     * 修改菜单权限
     *
     * @param menu 菜单权限
     * @return 结果
     */
    public int updateMenu(Menu menu);

    /**
     * 删除菜单权限
     *
     * @param menuId 菜单权限ID
     * @return 结果
     */
    public int deleteMenuById(Integer menuId);

    /**
     * 批量删除菜单权限
     *
     * @param menuIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMenuByIds(Integer[] menuIds);

    List<String> selectMenuPermsByUserId(Integer userId);

    List<Menu> selectMenuTreeByUserId(Integer userId);

    List<Menu> selectMenuListByUserId(Menu menu);

    /**
     * 根据角色ID查询菜单树信息
     *
     * @param roleId            角色ID
     * @param menuCheckStrictly 菜单树选择项是否关联显示
     * @return 选中菜单列表
     */
    List<Integer> selectMenuListByRoleId(@Param("roleId") Long roleId, @Param("menuCheckStrictly") boolean menuCheckStrictly);

    /**
     * 校验菜单名称是否唯一
     *
     * @param menuName 菜单名称
     * @param parentId 父菜单ID
     * @return 结果
     */
    Menu checkMenuNameUnique(@Param("menuName") String menuName, @Param("parentId") Integer parentId);

    /**
     * 是否存在菜单子节点
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    public int hasChildByMenuId(Integer menuId);
}