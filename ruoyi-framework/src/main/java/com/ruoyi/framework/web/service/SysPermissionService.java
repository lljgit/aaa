package com.ruoyi.framework.web.service;

import java.util.HashSet;
import java.util.Set;

import com.ruoyi.common.core.domain.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ruoyi.system.service.IMenuService;
import com.ruoyi.system.service.IRoleService;

/**
 * 用户权限处理
 *
 * @author ruoyi
 */
@Component
public class SysPermissionService {
    @Autowired
    private IRoleService roleService;

    @Autowired
    private IMenuService menuService;

    /**
     * 获取角色数据权限
     *
     * @param user 用户信息
     * @return 角色权限信息
     */
    public Set<String> getRolePermission(User user) {

        return roleService.selectRolePermissionByUserId(user.getUserId());
    }

    /**
     * 获取菜单数据权限
     *
     * @param user 用户信息
     * @return 菜单权限信息
     */
    public Set<String> getMenuPermission(User user) {
        Set<String> perms = new HashSet<>();
        perms.addAll(menuService.selectMenuPermsByUserId(user.getUserId()));
        return perms;
    }
}
